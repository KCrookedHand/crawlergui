package com.packt.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;

import java.util.List;

import com.packt.crawler.Student;

public class CustomBarChart extends AnchorPane {

	private NumberAxis lineYaxis = new NumberAxis();
	private CategoryAxis lineXaxis = new CategoryAxis();
	private BarChart marksDistribution = new BarChart(lineXaxis, lineYaxis);
	private XYChart.Data<String, Integer> d1 = new XYChart.Data<>("2.0", 0);
	private XYChart.Data<String, Integer> d2 = new XYChart.Data<>("3.0", 0);
	private XYChart.Data<String, Integer> d3 = new XYChart.Data<>("3,5", 0);
	private XYChart.Data<String, Integer> d4 = new XYChart.Data<>("4.0", 0);
	private XYChart.Data<String, Integer> d5 = new XYChart.Data<>("4,5", 0);
	private XYChart.Data<String, Integer> d6 = new XYChart.Data<>("5.0", 0);
	XYChart.Series<String, Integer> series = new XYChart.Series<>();

	public CustomBarChart() {
		series.getData().addAll(d1, d2, d3, d4, d5, d6);
		series.setName("MARK");
		lineXaxis.setLabel("MARK");
		lineYaxis.setAutoRanging(false);
		lineYaxis.setLowerBound(0);
		lineYaxis.setTickUnit(1);
		lineYaxis.setLabel("COUNT");
		marksDistribution.setTitle("Distribution of marks");
		marksDistribution.getData().add(series);
		this.getChildren().addAll(marksDistribution);
		this.setLeftAnchor(marksDistribution, 0.0);
		this.setRightAnchor(marksDistribution, 0.0);
	}

	public void setLineYaxis(List<Student> list) {
		lineYaxis.setUpperBound(list.size() + 1);
		for (Student el : list)
			switch (String.valueOf(el.getMark())) {
			case "2.0":
				d1.setYValue(d1.getYValue() + 1);
				break;
			case "3.0":
				d2.setYValue(d2.getYValue() + 1);
				break;
			case "3.5":
				d3.setYValue(d3.getYValue() + 1);
				break;
			case "4.0":
				d4.setYValue(d4.getYValue() + 1);
				break;
			case "4.5":
				d5.setYValue(d5.getYValue() + 1);
				break;
			case "5.0":
				d6.setYValue(d6.getYValue() + 1);
				break;
			default:
				break;
			}
	}

	public void addStudent(Student st) {
		lineYaxis.setUpperBound(lineYaxis.getUpperBound() + 1);
		switch (String.valueOf(st.getMark())) {
		case "2.0":
			d1.setYValue(d1.getYValue() + 1);
			break;
		case "3.0":
			d2.setYValue(d2.getYValue() + 1);
			break;
		case "3.5":
			d3.setYValue(d3.getYValue() + 1);
			break;
		case "4.0":
			d4.setYValue(d4.getYValue() + 1);
			break;
		case "4.5":
			d5.setYValue(d5.getYValue() + 1);
			break;
		case "5.0":
			d6.setYValue(d6.getYValue() + 1);
			break;
		default:
			break;
		}
	}

	public void removeStudent(Student st) {
		lineYaxis.setUpperBound(lineYaxis.getUpperBound() - 1);
		switch (String.valueOf(st.getMark())) {
		case "2.0":
			d1.setYValue(d1.getYValue() - 1);
			break;
		case "3.0":
			d2.setYValue(d2.getYValue() - 1);
			break;
		case "3.5":
			d3.setYValue(d3.getYValue() - 1);
			break;
		case "4.0":
			d4.setYValue(d4.getYValue() - 1);
			break;
		case "4.5":
			d5.setYValue(d5.getYValue() - 1);
			break;
		case "5.0":
			d6.setYValue(d6.getYValue() - 1);
			break;
		default:
			break;
		}
	}

	public void setLineXaxis() {
		ObservableList<String> marksList = FXCollections.observableArrayList("2.0", "3.0", "3,5", "4.0", "4,5", "5.0");
		lineXaxis.setCategories(marksList);

	}
}
