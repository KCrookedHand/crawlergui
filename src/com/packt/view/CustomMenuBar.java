package com.packt.view;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;

public class CustomMenuBar extends javafx.scene.control.MenuBar {

	private Menu programMenu = new Menu("Program");
	private Menu aboutMenu = new Menu("About");
	private MenuItem closeItem = new MenuItem("Close");
	private MenuItem aboutItem = new MenuItem("About");

	private void ShowAlert() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("About");
		alert.setHeaderText("Informations about CRAWLER Project");
		alert.setContentText("Created by: Krzyworączka Kamil");
		alert.showAndWait();
	}

	public CustomMenuBar() {
		closeItem.setAccelerator(KeyCombination.keyCombination("Ctrl+c"));
		closeItem.setOnAction(event -> Platform.exit());
		aboutItem.setOnAction(event -> ShowAlert());
		aboutMenu.getItems().add(aboutItem);
		programMenu.getItems().add(closeItem);
		this.getMenus().addAll(programMenu, aboutMenu);

	}
}
