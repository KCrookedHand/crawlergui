package com.packt.view;

import java.io.IOException;

import com.packt.crawler.GUILogger;
import com.packt.crawler.Student;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

public class CustomTabPane extends AnchorPane {

	private TabPane tabPane = new TabPane();
	private Tab studentsTab = new Tab("Students");
	private Tab logsTab = new Tab("Log");
	private Tab histogramTab = new Tab("Histogram");
	private CustomTableView studentsTable = new CustomTableView();

	private ObservableList<Student> studentsList;
	private GUILogger guiLogger = new GUILogger();
	private CustomBarChart markChart = new CustomBarChart();

	public void addChangeListener() {
		studentsList.addListener((ListChangeListener<Student>) c -> {
			while (c.next()) {
				if (c.wasAdded())
					for (Student el : c.getAddedSubList()) {
						guiLogger.log("NEW", el);
						markChart.addStudent(el);
					}

				if (c.wasRemoved())
					for (Student el : c.getRemoved()) {
						guiLogger.log("REMOVED", el);
						markChart.removeStudent(el);
					}
			}
		});
	}

	public void addStudent(Student st) {
		studentsList.add(st);
	}

	public void removeStudent() {
		if (studentsList.size() != 0)
			studentsList.remove(studentsList.size() - 1);
	}

	public CustomTabPane(ObservableList<Student> list) throws IOException {

		studentsList = list;
		studentsTable.table.setItems(studentsList);
		studentsTab.setContent(studentsTable);
		logsTab.setContent(guiLogger);
		markChart.setLineYaxis(studentsList);
		markChart.setLineXaxis();
		histogramTab.setContent(markChart);
		tabPane.getTabs().addAll(studentsTab, logsTab, histogramTab);
		this.getChildren().add(tabPane);
		this.setTopAnchor(tabPane, 0.0);
		this.setLeftAnchor(tabPane, 0.0);
		this.setRightAnchor(tabPane, 0.0);

	}

}
