package com.packt.view;

import com.packt.crawler.Student;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class CustomTableView extends AnchorPane {

	public TableView table = new TableView();
	private TableColumn markColumn = new TableColumn("Mark");
	private TableColumn firstNameColumn = new TableColumn("First Name");
	private TableColumn lastNameColumn = new TableColumn("Last Name");
	private TableColumn ageColumn = new TableColumn("Age");

	public CustomTableView() {
		markColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("mark"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("firstName"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("lastName"));
		ageColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("age"));
		
		table.setEditable(false);
		table.getColumns().addAll(markColumn, firstNameColumn, lastNameColumn, ageColumn);
		this.getChildren().addAll(table);
		this.setTopAnchor(table, 0.0);
		this.setLeftAnchor(table, 0.0);
		this.setRightAnchor(table, 0.0);
		this.setBottomAnchor(table, 0.0);
		markColumn.setMinWidth(150);
		firstNameColumn.setMinWidth(150);
		lastNameColumn.setMinWidth(150);
		ageColumn.setMinWidth(150);

	}

}
