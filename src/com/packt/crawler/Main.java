package com.packt.crawler;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

import com.packt.view.CustomMenuBar;
import com.packt.view.CustomTabPane;

public class Main extends Application {

	public static void main(String[] args) throws IOException {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException, InterruptedException {
		ObservableList<Student> students = FXCollections
				.observableArrayList(StudentsParser.parse(new File("C:\\Users\\Krzywy\\Desktop\\students.txt")));
		primaryStage.setTitle("Project CRAWLER");
		AnchorPane root = new AnchorPane();
		Scene scene = new Scene(root, primaryStage.getWidth(), primaryStage.getHeight(), Color.WHITE);

		CustomTabPane tab = new CustomTabPane(students);
		tab.addChangeListener();

		CustomMenuBar menuBar = new CustomMenuBar();

		Button addButton = new Button("Add student");
		addButton.setOnAction((event) -> {
			Student student = new Student();
			student.setFirstName("Jan");
			student.setLastName("Kowalski");
			student.setAge(22);
			student.setMark(4.0);
			tab.addStudent(student);
		});
		Button removeButton = new Button("Remove student");
		removeButton.setOnAction((event) -> {
			tab.removeStudent();
		});

		HBox hBox = new HBox(10.0, addButton, removeButton);

		root.getChildren().addAll(menuBar, tab, hBox);
		root.setTopAnchor(menuBar, 0.0);
		root.setLeftAnchor(menuBar, 0.0);
		root.setRightAnchor(menuBar, 0.0);
		root.setTopAnchor(tab, 25.5);
		root.setLeftAnchor(tab, 0.0);
		root.setRightAnchor(tab, 0.0);
		root.setBottomAnchor(hBox, 0.0);

		primaryStage.setScene(scene);
		primaryStage.show();

	}

}