package com.packt.crawler;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GUILogger extends VBox {
	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	public void log(String status, Student student) {

		String studentDescription = student.getMark() + "   " + student.getFirstName() + "   " + student.getLastName()
				+ "   " + student.getAge();
		this.getChildren().add(new Label(
				dateFormat.format(Calendar.getInstance().getTime()) + "   " + status + "   " + studentDescription));
	}

}
